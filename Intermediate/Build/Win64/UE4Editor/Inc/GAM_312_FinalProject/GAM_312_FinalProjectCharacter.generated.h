// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM_312_FINALPROJECT_GAM_312_FinalProjectCharacter_generated_h
#error "GAM_312_FinalProjectCharacter.generated.h already included, missing '#pragma once' in GAM_312_FinalProjectCharacter.h"
#endif
#define GAM_312_FINALPROJECT_GAM_312_FinalProjectCharacter_generated_h

#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_RPC_WRAPPERS
#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAM_312_FinalProjectCharacter(); \
	friend GAM_312_FINALPROJECT_API class UClass* Z_Construct_UClass_AGAM_312_FinalProjectCharacter(); \
public: \
	DECLARE_CLASS(AGAM_312_FinalProjectCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_312_FinalProject"), NO_API) \
	DECLARE_SERIALIZER(AGAM_312_FinalProjectCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGAM_312_FinalProjectCharacter(); \
	friend GAM_312_FINALPROJECT_API class UClass* Z_Construct_UClass_AGAM_312_FinalProjectCharacter(); \
public: \
	DECLARE_CLASS(AGAM_312_FinalProjectCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAM_312_FinalProject"), NO_API) \
	DECLARE_SERIALIZER(AGAM_312_FinalProjectCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAM_312_FinalProjectCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM_312_FinalProjectCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM_312_FinalProjectCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM_312_FinalProjectCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM_312_FinalProjectCharacter(AGAM_312_FinalProjectCharacter&&); \
	NO_API AGAM_312_FinalProjectCharacter(const AGAM_312_FinalProjectCharacter&); \
public:


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM_312_FinalProjectCharacter(AGAM_312_FinalProjectCharacter&&); \
	NO_API AGAM_312_FinalProjectCharacter(const AGAM_312_FinalProjectCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM_312_FinalProjectCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM_312_FinalProjectCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAM_312_FinalProjectCharacter)


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AGAM_312_FinalProjectCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AGAM_312_FinalProjectCharacter, FollowCamera); }


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_9_PROLOG
#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_RPC_WRAPPERS \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_INCLASS \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_INCLASS_NO_PURE_DECLS \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
