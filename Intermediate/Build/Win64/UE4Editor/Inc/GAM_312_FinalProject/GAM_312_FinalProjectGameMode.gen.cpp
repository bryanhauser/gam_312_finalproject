// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "GAM_312_FinalProjectGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGAM_312_FinalProjectGameMode() {}
// Cross Module References
	GAM_312_FINALPROJECT_API UClass* Z_Construct_UClass_AGAM_312_FinalProjectGameMode_NoRegister();
	GAM_312_FINALPROJECT_API UClass* Z_Construct_UClass_AGAM_312_FinalProjectGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_GAM_312_FinalProject();
// End Cross Module References
	void AGAM_312_FinalProjectGameMode::StaticRegisterNativesAGAM_312_FinalProjectGameMode()
	{
	}
	UClass* Z_Construct_UClass_AGAM_312_FinalProjectGameMode_NoRegister()
	{
		return AGAM_312_FinalProjectGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AGAM_312_FinalProjectGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_GAM_312_FinalProject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "GAM_312_FinalProjectGameMode.h" },
				{ "ModuleRelativePath", "GAM_312_FinalProjectGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AGAM_312_FinalProjectGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AGAM_312_FinalProjectGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGAM_312_FinalProjectGameMode, 1903506197);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGAM_312_FinalProjectGameMode(Z_Construct_UClass_AGAM_312_FinalProjectGameMode, &AGAM_312_FinalProjectGameMode::StaticClass, TEXT("/Script/GAM_312_FinalProject"), TEXT("AGAM_312_FinalProjectGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGAM_312_FinalProjectGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
