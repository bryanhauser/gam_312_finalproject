// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM_312_FINALPROJECT_GAM_312_FinalProjectGameMode_generated_h
#error "GAM_312_FinalProjectGameMode.generated.h already included, missing '#pragma once' in GAM_312_FinalProjectGameMode.h"
#endif
#define GAM_312_FINALPROJECT_GAM_312_FinalProjectGameMode_generated_h

#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_RPC_WRAPPERS
#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAM_312_FinalProjectGameMode(); \
	friend GAM_312_FINALPROJECT_API class UClass* Z_Construct_UClass_AGAM_312_FinalProjectGameMode(); \
public: \
	DECLARE_CLASS(AGAM_312_FinalProjectGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/GAM_312_FinalProject"), GAM_312_FINALPROJECT_API) \
	DECLARE_SERIALIZER(AGAM_312_FinalProjectGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGAM_312_FinalProjectGameMode(); \
	friend GAM_312_FINALPROJECT_API class UClass* Z_Construct_UClass_AGAM_312_FinalProjectGameMode(); \
public: \
	DECLARE_CLASS(AGAM_312_FinalProjectGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/GAM_312_FinalProject"), GAM_312_FINALPROJECT_API) \
	DECLARE_SERIALIZER(AGAM_312_FinalProjectGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAM_312_FINALPROJECT_API AGAM_312_FinalProjectGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM_312_FinalProjectGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAM_312_FINALPROJECT_API, AGAM_312_FinalProjectGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM_312_FinalProjectGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAM_312_FINALPROJECT_API AGAM_312_FinalProjectGameMode(AGAM_312_FinalProjectGameMode&&); \
	GAM_312_FINALPROJECT_API AGAM_312_FinalProjectGameMode(const AGAM_312_FinalProjectGameMode&); \
public:


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAM_312_FINALPROJECT_API AGAM_312_FinalProjectGameMode(AGAM_312_FinalProjectGameMode&&); \
	GAM_312_FINALPROJECT_API AGAM_312_FinalProjectGameMode(const AGAM_312_FinalProjectGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAM_312_FINALPROJECT_API, AGAM_312_FinalProjectGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM_312_FinalProjectGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAM_312_FinalProjectGameMode)


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_9_PROLOG
#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_RPC_WRAPPERS \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_INCLASS \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_INCLASS_NO_PURE_DECLS \
	GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM_312_FinalProject_Source_GAM_312_FinalProject_GAM_312_FinalProjectGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
