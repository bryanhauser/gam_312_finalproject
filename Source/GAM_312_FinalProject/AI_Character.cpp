// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_Character.h"
#include "AI_Controller.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


// Sets default values
AAI_Character::AAI_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Set base stat values
	SetCurrentLevel(1);
	SetTotalHealth(10.f);
	SetTotalStamina(10.f);
	SetTotalMana(10.f);
	CurrentHealth = TotalHealth;
	CurrentStamina = TotalStamina;
	CurrentMana = TotalMana;
}

// Called when the game starts or when spawned
void AAI_Character::BeginPlay()
{
	Super::BeginPlay();
	

}

// Called every frame
void AAI_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentHealth <= 0.f)
	{
		CurrentHealth = 0.f;
		OnDeath();
	}
}

// Called to bind functionality to input
void AAI_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AAI_Character::OnDeath()
{
	Destroy();
}

//Getters and setters for CURRENT stats
float AAI_Character::GetCurrentHealth()
{
	return CurrentHealth;
}

void AAI_Character::SetCurrentHealth(float Health)
{
	CurrentHealth = CurrentHealth + Health;

	if (CurrentHealth > TotalHealth)
	{
		CurrentHealth = TotalHealth;
	}
}

float AAI_Character::GetCurrentStamina()
{
	return CurrentStamina;
}

void AAI_Character::SetCurrentStamina(float Stamina)
{
	CurrentStamina = CurrentStamina + Stamina;
}

float AAI_Character::GetCurrentMana()
{
	return CurrentMana;
}

void AAI_Character::SetCurrentMana(float Mana)
{
	CurrentMana = CurrentMana + Mana;
}

int AAI_Character::GetCurrentLevel()
{
	return CurrentLevel;
}

void AAI_Character::SetCurrentLevel(int Level)
{
	CurrentLevel = CurrentLevel + Level;
}

//Getters and setters for TOTAL stats
float AAI_Character::GetTotalHealth()
{
	return TotalHealth;
}

void AAI_Character::SetTotalHealth(float Health)
{
	TotalHealth = TotalHealth + Health;
}

float AAI_Character::GetTotalStamina()
{
	return TotalStamina;
}

void AAI_Character::SetTotalStamina(float Stamina)
{
	TotalStamina = TotalStamina + Stamina;
}

float AAI_Character::GetTotalMana()
{
	return TotalMana;
}

void AAI_Character::SetTotalMana(float Mana)
{
	TotalMana = TotalMana + Mana;
}