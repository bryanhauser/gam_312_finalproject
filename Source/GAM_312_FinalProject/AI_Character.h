// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AI_Character.generated.h"

UCLASS()
class GAM312PROJECT_API AAI_Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAI_Character();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	//Stat values
	UPROPERTY(VisibleAnywhere, Category = "Enemy Stats")
	float CurrentHealth;
	UPROPERTY(VisibleAnywhere, Category = "Enemy Stats")
	float TotalHealth;
	UPROPERTY(VisibleAnywhere, Category = "Enemy Stats")
	float CurrentStamina;
	UPROPERTY(VisibleAnywhere, Category = "Enemy Stats")
	float TotalStamina;
	UPROPERTY(VisibleAnywhere, Category = "Enemy Stats")
	float CurrentMana;
	UPROPERTY(VisibleAnywhere, Category = "Enemy Stats")
	float TotalMana;
	UPROPERTY(VisibleAnywhere, Category = "Enemy Stats")
	int CurrentLevel;

public:

	//Getter and setter functions for stats
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	void SetCurrentHealth(float Health);
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	float GetCurrentStamina();
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	void SetCurrentStamina(float Stamina);
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	float GetCurrentMana();
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	void SetCurrentMana(float Mana);
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	int GetCurrentLevel();
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	void SetCurrentLevel(int Level);
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	float GetTotalHealth();
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	void SetTotalHealth(float Health);
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	float GetTotalStamina();
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	void SetTotalStamina(float Stamina);
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	float GetTotalMana();
	UFUNCTION(BlueprintCallable, Category = "Enemy Stats")
	void SetTotalMana(float Mana);
	UFUNCTION(BlueprintCallable, Category = "Enemy Status")
	void OnDeath();
};
