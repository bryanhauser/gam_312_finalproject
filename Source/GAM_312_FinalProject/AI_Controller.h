// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "AIController.h"
#include "AI_Controller.generated.h"

/**
 * 
 */
UCLASS()
class GAM312PROJECT_API AAI_Controller : public AAIController
{
	GENERATED_BODY()


public:

	void BeginPlay() override;

private:

	//Get random wayponint function
	UFUNCTION()
	ATargetPoint* GetRandomWaypoint();

	//Waypoint array
	UPROPERTY()
	TArray<AActor*> Waypoints;

	//Function to tell AI to move
	UFUNCTION()
	void GoToRandomWaypoint();
};
