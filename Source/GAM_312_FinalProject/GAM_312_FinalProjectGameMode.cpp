// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "GAM_312_FinalProjectGameMode.h"
#include "GAM_312_FinalProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGAM_312_FinalProjectGameMode::AGAM_312_FinalProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
