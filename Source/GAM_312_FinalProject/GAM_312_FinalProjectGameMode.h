// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAM_312_FinalProjectGameMode.generated.h"

UCLASS(minimalapi)
class AGAM_312_FinalProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAM_312_FinalProjectGameMode();
};



